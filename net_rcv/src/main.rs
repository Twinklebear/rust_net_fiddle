extern crate cbor;
extern crate docopt;
extern crate "rustc-serialize" as rustc_serialize;

use std::net;
use std::net::{TcpListener, TcpStream};
use std::io::prelude::*;
use cbor::Decoder;
use docopt::Docopt;

#[derive(RustcEncodable, RustcDecodable, Debug)]
struct Colorf {
	r: f32,
	g: f32,
	b: f32,
}

#[derive(RustcEncodable, RustcDecodable, Debug)]
struct Sample {
	x: f32,
	y: f32,
	color: Colorf,
}

static USAGE: &'static str = "
Usage: net_connect <host> <port>
";

#[derive(RustcDecodable, Debug)]
struct Args {
    arg_host: String,
    arg_port: u16,
}

fn handle_client(mut stream: TcpStream) {
    let mut res: Vec<u8> = Vec::new();
    match stream.read_to_end(&mut res) {
        Err(e) => println!("Error reading, {}", e),
        _ => {},
    }
    let mut dec = Decoder::from_bytes(&res[..]);
    // Is there a way to directly use the stream? The stream
    // implements read so why can't we use it?
    //let mut dec = Decoder::from_reader(stream);
    for s in dec.decode::<Sample>() {
        if let Ok(samp) = s {
            println!("Decoded sample {:?}", samp);
        }
    }
}

fn main() {
    let args: Args = Docopt::new(USAGE).and_then(|d| d.decode()).unwrap_or_else(|e| e.exit());
    println!("Args: {:?}", args);

    let listener = TcpListener::bind(&(&args.arg_host[..], args.arg_port)).unwrap();
    for stream in listener.incoming() {
        match stream {
            Ok(stream) => handle_client(stream),
            Err(e) => println!("Error connecting, {}", e),
        }
    }
}

