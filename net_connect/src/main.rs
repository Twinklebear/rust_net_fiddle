extern crate cbor;
extern crate docopt;
extern crate "rustc-serialize" as rustc_serialize;

use std::net;
use std::net::TcpStream;
use std::io::prelude::*;
use cbor::Encoder;
use docopt::Docopt;

#[derive(RustcEncodable, RustcDecodable, Debug)]
struct Colorf {
	r: f32,
	g: f32,
	b: f32,
}

#[derive(RustcEncodable, RustcDecodable, Debug)]
struct Sample {
	x: f32,
	y: f32,
	color: Colorf,
}

static USAGE: &'static str = "
Usage: net_connect <host> <port>
";

#[derive(RustcDecodable, Debug)]
struct Args {
    arg_host: String,
    arg_port: u16,
}

fn main() {
    let args: Args = Docopt::new(USAGE).and_then(|d| d.decode()).unwrap_or_else(|e| e.exit());
    println!("Args: {:?}", args);
    let mut target_host = String::new();
    for host in net::lookup_host(&args.arg_host[..]).unwrap() {
        match host {
            Ok(h) => {
                target_host = h.to_string();
                // pop off the :0 part of the host
                target_host.pop();
                target_host.pop();
                println!("Found host {}", target_host);
            },
            Err(e) => println!("Error finding host {}", e),
        }
    }
    let mut stream = match TcpStream::connect(&(&target_host[..], args.arg_port)) {
        Ok(s) => s,
        Err(e) => panic!("Connection failed: {}", e),
    };
    let samples = vec![Sample { x: 0.0, y: 1.0, color: Colorf { r: 1.0, g: 0.5, b: 0.2 } }];
    // Is there a way to directly use the stream? The stream
    // implements write so why can't we use it?
    let mut encoder = Encoder::from_memory();
    match encoder.encode(samples) {
        Err(e) => println!("Failure encoding: {}", e),
        _ => {},
    }
    match stream.write_all(encoder.as_bytes()) {
        Err(e) => println!("Failure sending: {}", e),
        _ => {},
    }
}

